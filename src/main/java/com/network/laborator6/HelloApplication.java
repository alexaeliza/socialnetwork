package com.network.laborator6;

import com.network.laborator6.domain.validator.UserValidator;
import com.network.laborator6.gui.LoginController;
import com.network.laborator6.repository.database.*;
import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import com.network.laborator6.utils.Constants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login-view.fxml"));
        RepositoryUserDatabase repositoryUserDatabase = new RepositoryUserDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        RepositoryFriendshipDatabase repositoryFriendshipDatabase = new RepositoryFriendshipDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        RepositoryUserLoginDatabase repositoryUserLoginDatabase = new RepositoryUserLoginDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        RepositoryFriendRequestDatabase repositoryFriendRequestDatabase = new RepositoryFriendRequestDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        RepositoryChatDatabase repositoryChatDatabase = new RepositoryChatDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        RepositoryMessageDatabase repositoryMessageDatabase = new RepositoryMessageDatabase(Constants.URL_POSTGRES, Constants.USERNAME_POSTGRES, Constants.PASSWORD_POSTGRES);
        UserValidator userValidator = new UserValidator();

        ServiceUser serviceUser = new ServiceUser(repositoryUserDatabase, repositoryFriendshipDatabase, repositoryUserLoginDatabase, userValidator);
        ServiceFriendRequest serviceFriendRequest = new ServiceFriendRequest(repositoryFriendRequestDatabase, repositoryUserDatabase, repositoryFriendshipDatabase);
        ServiceFriendship serviceFriendship = new ServiceFriendship(repositoryUserDatabase, repositoryFriendshipDatabase, repositoryFriendRequestDatabase);
        ServiceChat serviceChat = new ServiceChat(repositoryMessageDatabase, repositoryChatDatabase, repositoryUserDatabase);

        LoginController loginController = new LoginController(serviceUser, serviceFriendRequest, serviceFriendship, serviceChat);
        fxmlLoader.setController(loginController);

        Scene scene = new Scene(fxmlLoader.load(), 588, 370);
        stage.setTitle("Log in");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}