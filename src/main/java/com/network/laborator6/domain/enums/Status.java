package com.network.laborator6.domain.enums;

public enum Status {
    pending,
    approved,
    rejected
}
