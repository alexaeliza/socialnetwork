package com.network.laborator6.gui;

import com.network.laborator6.domain.User;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.gui.domainFXML.FriendshipFXML;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import com.network.laborator6.utils.events.FriendshipChangeEvent;
import com.network.laborator6.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

public class FriendshipsController implements Observer<FriendshipChangeEvent>, Initializable {

    @FXML
    private TableColumn<FriendshipFXML, Button> tableColumnAction;

    @FXML
    private TableColumn<FriendshipFXML, String> tableColumnFirstName;

    @FXML
    private TableColumn<FriendshipFXML, String> tableColumnLastName;

    @FXML
    private TableColumn<FriendshipFXML, String> tableColumnUsername;

    @FXML
    private TableColumn<FriendshipFXML, Circle> tableColumnPicture;

    @FXML
    private TableView<FriendshipFXML> tableViewFriends;


    private final ObservableList<FriendshipFXML> friendships = FXCollections.observableArrayList();
    private ServiceFriendship serviceFriendship;
    private ServiceUser serviceUser;
    private Long id;
    private String username;

    public FriendshipsController(ServiceFriendship serviceFriendship, ServiceUser serviceUser, Long id, String username) {
        this.serviceFriendship = serviceFriendship;
        this.serviceUser = serviceUser;
        this.id = id;
        this.username = username;
        serviceFriendship.addObserver(this);
        initTableView();
    }

    @Override
    public void update(FriendshipChangeEvent friendshipChangeEvent) {
        initTableView();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tableColumnPicture.setCellValueFactory(new PropertyValueFactory<>("picture"));
        tableColumnUsername.setCellValueFactory(new PropertyValueFactory<>("username"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableColumnAction.setCellValueFactory(new PropertyValueFactory<>("deleteButton"));
        tableViewFriends.setItems(friendships);
        tableViewFriends.setPlaceholder(new Label("No available friend"));
    }

    private void initTableView(){
        Map<User, LocalDate> friends = serviceFriendship.getFriends(this.id);
        List<FriendshipFXML> friendshipsFXML = new ArrayList<>();
        friends.forEach((user, date) -> {
            FriendshipFXML friendshipFXML = new FriendshipFXML(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(), date);
            Button deleteButton = null;
            try {
                deleteButton = getDeleteButton(friendshipFXML.getId());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            friendshipFXML.setDeleteButton(deleteButton);
            friendshipFXML.setPicture(getPictureUser(friendshipFXML.getId()));
            friendshipsFXML.add(friendshipFXML);
        });
        this.friendships.setAll(friendshipsFXML);
    }

    private Button getDeleteButton(Long idFriends) throws MalformedURLException {
        Button deleteButton = new Button("");
        deleteButton.setId("btnDelete");
        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setContentText("Are you sure you want to delete this friend?");
                alert.setTitle("Confirmation");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK)
                    serviceFriendship.removeFriend(id, idFriends);
                else
                    tableViewFriends.setSelectionModel(null);
            }
        });

        return deleteButton;
    }

    private Circle getPictureUser(Long id){
        Circle circlePicture = new Circle();
        circlePicture.setRadius(20);
        UserLogin userLogin = serviceUser.findUserLoginById(id);
        File file = new File(userLogin.getImage());
        try {
            Image imgUser = new Image(file.toURI().toURL().toExternalForm());
            circlePicture.setFill(new ImagePattern(imgUser));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return circlePicture;
    }


}
