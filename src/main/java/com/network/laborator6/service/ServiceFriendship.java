package com.network.laborator6.service;

import com.network.laborator6.domain.Friendship;
import com.network.laborator6.domain.Pair;
import com.network.laborator6.domain.User;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.repository.database.RepositoryFriendRequestDatabase;
import com.network.laborator6.repository.database.RepositoryFriendshipDatabase;
import com.network.laborator6.repository.database.RepositoryUserDatabase;
import com.network.laborator6.utils.events.EventType;
import com.network.laborator6.utils.events.FriendshipChangeEvent;
import com.network.laborator6.utils.observer.Observable;
import com.network.laborator6.utils.observer.Observer;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ServiceFriendship implements Observable<FriendshipChangeEvent> {
    private final RepositoryUserDatabase repositoryUsers;
    private final RepositoryFriendshipDatabase repositoryFriendships;
    private final RepositoryFriendRequestDatabase repositoryFriendRequest;
    private List<Observer<FriendshipChangeEvent>> observers;

    public ServiceFriendship(RepositoryUserDatabase repositoryUsers, RepositoryFriendshipDatabase repositoryFriendships, RepositoryFriendRequestDatabase repositoryFriendRequest) {
        this.repositoryUsers = repositoryUsers;
        this.repositoryFriendships = repositoryFriendships;
        this.repositoryFriendRequest = repositoryFriendRequest;
        observers = new ArrayList<>();
    }

    public Iterable<User> getUsers() {
        return repositoryUsers.getAll();
    }

    /**
     *
     * @return a list of all friendships in the repository
     */
    public Iterable<Friendship> getFriendships() {
        return repositoryFriendships.getAll();
    }


    /**
     *
     * @param friend1Id the id of the first friend
     * @param friend2Id the id of the second friend
     */
    public void addFriend(Long friend1Id, Long friend2Id) {
        if (friend1Id.equals(friend2Id))
            throw new ServiceException("\nFriendship needs 2 different friends");
        Friendship friendship = new Friendship(friend1Id, friend2Id);
        if (repositoryFriendships.add(friendship) != null)
            throw new ServiceException("\nFriendship already exists\n");
    }

    /**
     *
     * @param friend1Id the id of the first friend
     * @param friend2Id the id of the second friend
     */
    public void removeFriend(Long friend1Id, Long friend2Id) {
        Friendship friendship = repositoryFriendships.getById(new Pair<>(friend1Id, friend2Id));
        Friendship newFriendship = repositoryFriendships.remove(friendship.getId());
        if (newFriendship == null)
            throw new ServiceException("\nFriendship does not exist\n");
        repositoryFriendRequest.remove(new Pair<>(friend1Id, friend2Id));
        repositoryFriendRequest.remove(new Pair<>(friend2Id, friend1Id));
        notifyObservers(new FriendshipChangeEvent(EventType.UPDATE ,friendship, newFriendship));
    }

    /**
     *
     * @return a pair of the numbers of communities and a map of the communities
     */
    public Pair<Integer, Map<Integer, List<User>>> getCommunities() {
        Graph graph = new Graph();
        graph.buildGraph(getUsers(), getFriendships());
        List<List<Long>> components = graph.getCommunities();
        Map<Integer, List<User>> map = new HashMap<>();
        int[] k = new int[1];
        k[0] = 1;
        components.forEach(component -> {
            List<User> users = new ArrayList<>();
            component.forEach(id -> users.add(repositoryUsers.getById(id)));
            map.put(k[0]++, users);
        });
        return new Pair<>(graph.getNumberCommunities(), map);
    }

    /**
     *
     * @return a list of users that form the largest commuity
     */
    public List<User> getLargestCommunity() {
        Graph graph = new Graph();
        graph.buildGraph(getUsers(), getFriendships());
        List<Long> ids = graph.getBiggestCommunity();
        List<User> users = new ArrayList<>();
        ids.forEach(id -> users.add(repositoryUsers.getById(id)));
        return users;
    }

    /**
     *
     * @param id - the id for which the friends are searched
     * @return a Map of friends
     */
    public Map<User, LocalDate> getFriends(Long id) {
        List<Friendship> friendships = (List<Friendship>) repositoryFriendships.getAll();

        Map<User, LocalDate> friendsRight = friendships.stream()
                .filter(friendship -> Objects.equals(friendship.getId().getFirst(), id))
                .collect(Collectors.toMap(friendship -> repositoryUsers.getById(friendship.getSecond()), Friendship::getFriendshipDate));
        Map<User, LocalDate> friendsLeft = friendships.stream()
                .filter(friendship -> Objects.equals(friendship.getId().getSecond(), id))
                .collect(Collectors.toMap(friendship -> repositoryUsers.getById(friendship.getFirst()), Friendship::getFriendshipDate));

        friendsRight.putAll(friendsLeft);
        return friendsRight;
    }

    /**
     *
     * @param id - the id for which the friends are searched
     * @param month - the month we search for
     * @return a Map containg the friends
     */
    public Map<User, LocalDate> getFriendshipsByMonth(Long id, String month) {
        String MONTH = month.toUpperCase(Locale.ROOT);
        return getFriends(id).entrySet()
                .stream()
                .filter(friendship -> friendship.getValue().getMonth().equals(Month.valueOf(MONTH)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public int getNumberFriendships(Long id, LocalDate startDate, LocalDate endDate) {
        Map<User, LocalDate> friendships = getFriends(id);
        AtomicInteger counter = new AtomicInteger();
        friendships.forEach((user, date) -> {
            if (date.isAfter(startDate) && date.isBefore(endDate))
                counter.getAndIncrement();
        });
        return counter.get();
    }

    @Override
    public void addObserver(Observer<FriendshipChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipChangeEvent t) {
        observers.forEach(x -> x.update(t));
    }
}
