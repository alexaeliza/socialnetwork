package com.network.laborator6.domain;

import com.network.laborator6.domain.enums.Status;

import java.time.LocalDate;
import java.util.Objects;

public class FriendRequest extends Entity<Pair<Long, Long>> {
    LocalDate date;
    Status status;

    public FriendRequest(Long id1, Long id2) {
        this.setId(new Pair<>(id1, id2));
        date = LocalDate.now();
        status = Status.pending;
    }

    public FriendRequest(Long id1, Long id2, LocalDate date, Status status) {
        this.setId(new Pair<>(id1, id2));
        this.date = date;
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FriendRequest that = (FriendRequest) o;
        return Objects.equals(this.getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
        return "Id: " + id +
                "\nSent date: " + date +
                "\nCurrent status: " + status;
    }
}
