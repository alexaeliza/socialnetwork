package com.network.laborator6.utils.events;

public enum EventType {
    ADD,
    DELETE,
    UPDATE
}
