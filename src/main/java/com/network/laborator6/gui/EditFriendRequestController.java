package com.network.laborator6.gui;

import com.network.laborator6.gui.domainFXML.FriendFXML;
import com.network.laborator6.domain.User;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceUser;
import com.network.laborator6.utils.events.FriendRequestChangeEvent;
import com.network.laborator6.utils.observer.Observer;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class EditFriendRequestController implements Observer<FriendRequestChangeEvent>, Initializable {
    private final Long id;
    private final String username;
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceUser serviceUser;
    private final ObservableList<FriendFXML> available = FXCollections.observableArrayList();

    @FXML
    TableView<FriendFXML> sendTable;

    @FXML
    TableColumn<FriendFXML, String> sendUsername;

    @FXML
    TableColumn<FriendFXML, String> sendFirstName;

    @FXML
    TableColumn<FriendFXML, String> sendLastName;

    @FXML
    TableColumn<FriendFXML, Circle> sendPicture;

    @FXML
    TableColumn<FriendFXML, Button> sendAction;

    @FXML
    TextField search;

    public EditFriendRequestController(Long id, String username, ServiceFriendRequest serviceFriendRequest, ServiceUser serviceUser) {
        this.id = id;
        this.username = username;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceUser = serviceUser;
        serviceFriendRequest.addObserver(this);
        init();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sendPicture.setCellValueFactory(new PropertyValueFactory<>("picture"));
        sendUsername.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getUsername()));
        sendFirstName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getFirstName()));
        sendLastName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getLastName()));
        sendAction.setCellValueFactory(new PropertyValueFactory<>("send"));
        sendTable.setItems(available);
        sendTable.setPlaceholder(new Label("No available users"));
        search.setPromptText("Search");
        search.textProperty().addListener(o -> handleInit());
    }

    private void handleInit() {
        List<User> users = serviceFriendRequest.getAvailableSentFriendRequests(id, search.getText());
        List<FriendFXML> friends = new ArrayList<>();
        users.forEach(user -> {
            FriendFXML friendFXML = new FriendFXML(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName());
            Button send = getSendButton(user.getId());
            Circle picture = getPicture(user.getId());
            friendFXML.setSend(send);
            friendFXML.setPicture(picture);
            friends.add(friendFXML);
        });
        available.setAll(friends);
    }

    public void init() {
        List<User> users = serviceFriendRequest.getAvailableSentFriendRequests(id, "");
        List<FriendFXML> friends = new ArrayList<>();
        users.forEach(user -> {
            FriendFXML friendFXML = new FriendFXML(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName());
            Button send = getSendButton(user.getId());
            Circle picture = getPicture(user.getId());
            friendFXML.setSend(send);
            friendFXML.setPicture(picture);
            friends.add(friendFXML);
        });
        available.setAll(friends);
    }

    public Button getSendButton(Long idUser) {
        Button button = new Button("");
        button.setId("btnSend");
        button.setOnAction(event -> serviceFriendRequest.sendFriendRequest(id, idUser));
        return button;
    }

    public Circle getPicture(Long id){
        Circle circlePicture = new Circle();
        circlePicture.setRadius(20);
        UserLogin userLogin = serviceUser.findUserLoginById(id);
        File file = new File(userLogin.getImage());
        try {
            Image imgUser = new Image(file.toURI().toURL().toExternalForm());
            circlePicture.setFill(new ImagePattern(imgUser));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return circlePicture;
    }

    @Override
    public void update(FriendRequestChangeEvent friendRequestChangeEvent) {
        init();
    }
}
