package com.network.laborator6.service;

import com.network.laborator6.domain.Chat;
import com.network.laborator6.domain.Message;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.repository.database.RepositoryChatDatabase;
import com.network.laborator6.repository.database.RepositoryMessageDatabase;
import com.network.laborator6.repository.database.RepositoryUserDatabase;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ServiceChat {
    private final RepositoryMessageDatabase messageRepository;
    private final RepositoryChatDatabase chatRepository;
    private final RepositoryUserDatabase userRepository;

    public ServiceChat(RepositoryMessageDatabase messageRepository, RepositoryChatDatabase chatRepository, RepositoryUserDatabase userRepository) {
        this.messageRepository = messageRepository;
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
    }

    /**
     *
     * @param idUser: Long
     * @return: a dictionary with all messages to which the user did not reply
     */
    public Map<Long, List<Message>> getAllNoReplyMessagesForAUser(Long idUser){
        Map<Long, List<Message>> allNoReplyMessages = new TreeMap<>();
        //the chats of which the user is a part
        List<Long> idChats = chatRepository.getIdChatsForAUser(idUser);
        idChats.forEach(idChat -> {
            Chat chat = chatRepository.getById(idChat);
            List<Long> idMessages = chat.getMessages();
            idMessages.sort(Comparator.reverseOrder());
            List<Message> noReplyMessages = new ArrayList<>();

            boolean stop = false;
            for (Long idMessage: idMessages) {
                Message message = messageRepository.getById(idMessage);
                if (Objects.equals(message.getIdFrom(), idUser))
                    stop = true;
                if (!stop)
                    noReplyMessages.add(message);
                if (stop && message.getReply() == 0 && !Objects.equals(message.getIdFrom(), idUser))
                    noReplyMessages.add(message);
            }

            Comparator<Message> messageComparator = (Message m1, Message m2) -> m2.getId().compareTo(m1.getId());
            noReplyMessages.sort(messageComparator.reversed());
            if (!noReplyMessages.isEmpty())
                allNoReplyMessages.put(idChat, noReplyMessages);
        });
        return allNoReplyMessages;
    }

    /**
     *
     * @param idChat: Long
     * @return: Chat if it exists, otherwise throw ServiceException
     */
    public Chat getChatById(Long idChat){
        Chat chat = chatRepository.getById(idChat);
        if (chat == null)
            throw new ServiceException("There is no chat with this id");
        return chat;
    }

    /**
     * Send a new message
     * Build a new chat if it doesn't exist, otherwise send the new message to the existing chat
     * @param from: Long
     * @param to: List of Long
     * @param message: String
     */
    public void sendANewMessage(Long from, List<Long> to, String message){
        List<Long> idUsers = new ArrayList<>(to);
        idUsers.add(from);
        existUsers(idUsers);
        Chat chat = findAChatByIdUsers(idUsers);
        if (chat == null){
            chat = new Chat(null, idUsers);
            chat.setId(chatRepository.getTheNextId());
            chatRepository.add(chat);
        }
        Message newMessage = new Message(from, chat.getId(), message, null);
        newMessage.setId(messageRepository.getTheNextId());
        messageRepository.add(newMessage);
    }

    /**
     * Responds to a message sent by a user
     * @param idFrom: Long
     * @param idMessageReply: Long
     * @param message: String
     */
    public void replyToAMessage(Long idFrom, Long idMessageReply, String message){
        if (messageRepository.getById(idMessageReply) == null)
            throw new ServiceException("There is no message with this id");
        List<Message> messagesNoReply = messageRepository.getNoReplyMessagesForAUser(idFrom);
        messagesNoReply = messagesNoReply.stream().filter(m -> Objects.equals(m.getId(), idMessageReply)).collect(Collectors.toList());
        if (messagesNoReply.isEmpty())
            throw new ServiceException("You have no reply messages with this id");
        Message messageNoReply = messagesNoReply.get(0);

        Long idMessageSend = messageRepository.getTheNextId();
        Long toIdChat = messageNoReply.getIdTo();
        Message messageSend = new Message(idFrom, toIdChat, message, null);
        messageSend.setId(idMessageSend);
        messageRepository.add(messageSend);

        messageNoReply.setReply(idMessageSend);
        messageRepository.update(messageNoReply);
    }

    /**
     *
     * @param idUsers: Long
     * @return: a list of all messages in a chat
     */
    public List<Message> getConversationUsers(List<Long> idUsers){
        existUsers(idUsers);
        Chat chat = findAChatByIdUsers(idUsers);
        if (chat == null)
            throw new ServiceException("There is no conversation between these users");
        List<Message> messages = new ArrayList<>();
        List<Long> idMessages = chat.getMessages();
        idMessages.forEach(idMessage -> messages.add(messageRepository.getById(idMessage)));
        return messages;
    }

    /**
     * Search for a chat that contains those users
     * @param idUsers: Long
     * @return: Chat if it exists, otherwise null
     */
    public Chat findAChatByIdUsers(List<Long> idUsers){
        Collections.sort(idUsers);
        Iterable<Chat> chats = chatRepository.getAll();
        for (Chat chat: chats) {
            List<Long> idUserFromChat = chat.getUsers();
            Collections.sort(idUserFromChat);
            if (idUserFromChat.equals(idUsers))
                return chat;
        }
        return null;
    }

    /**
     * Check if all users in the list exist
     * @param idUsers: Long
     */
    public void existUsers(List<Long> idUsers){
        idUsers.forEach(id -> {
            if (userRepository.getById(id) == null)
                throw new ServiceException("The user with id " + id + " does not exist");
        });
    }

    public int getNumberReceivedMessages(Long id, LocalDate startDate, LocalDate endDate) {
        List<Long> idChats = chatRepository.getIdChatsForAUser(id);
        AtomicInteger counter = new AtomicInteger();
        LocalDateTime startTime = startDate.atStartOfDay();
        LocalDateTime endTime = endDate.atStartOfDay();

        idChats.forEach(idChat -> {
            Chat chat = chatRepository.getById(idChat);
            List<Long> idMessages = chat.getMessages();

            for (Long idMessage: idMessages) {
                Message message = messageRepository.getById(idMessage);
                if (!Objects.equals(message.getIdFrom(), id) && message.getDateTime().isAfter(startTime) && message.getDateTime().isBefore(endTime))
                    counter.getAndIncrement();
            }
        });
        return counter.get();
    }

    public int getNumberSentMessages(Long id, LocalDate startDate, LocalDate endDate) {
        List<Long> idChats = chatRepository.getIdChatsForAUser(id);
        AtomicInteger counter = new AtomicInteger();
        LocalDateTime startTime = startDate.atStartOfDay();
        LocalDateTime endTime = endDate.atStartOfDay();

        idChats.forEach(idChat -> {
            Chat chat = chatRepository.getById(idChat);
            List<Long> idMessages = chat.getMessages();

            for (Long idMessage: idMessages) {
                Message message = messageRepository.getById(idMessage);
                if (Objects.equals(message.getIdFrom(), id) && message.getDateTime().isAfter(startTime) && message.getDateTime().isBefore(endTime))
                    counter.getAndIncrement();
            }
        });
        return counter.get();
    }
}
