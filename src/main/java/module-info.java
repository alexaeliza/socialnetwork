module com.network.laborator6 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;

    opens com.network.laborator6 to javafx.fxml;
    exports com.network.laborator6.gui;
    opens com.network.laborator6.gui to javafx.fxml;
    exports com.network.laborator6;
    exports com.network.laborator6.gui.domainFXML;
    opens com.network.laborator6.gui.domainFXML to javafx.fxml;
}