package com.network.laborator6.repository.database;

import com.network.laborator6.domain.User;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.repository.RepositoryEntity;

import java.io.File;
import java.sql.*;
import java.util.*;

public class RepositoryUserLoginDatabase implements RepositoryEntity<Long, UserLogin> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryUserLoginDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    protected Long getFirstUnusedId() {
        Map<Long, UserLogin> users = getAllMap();
        Long firstUnusedId = 1L;

        while (users.containsKey(firstUnusedId))
            ++firstUnusedId;
        return firstUnusedId;
    }

    public Map<Long, UserLogin> getAllMap() {
        Map<Long, UserLogin> usersMap = new HashMap<>();
        Iterable<UserLogin> users = getAll();
        users.forEach(user -> usersMap.put(user.getId(), user));
        return usersMap;
    }

    @Override
    public UserLogin getById(Long id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null");
        UserLogin user = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM \"userLogin\" WHERE \"idUser\" = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            String image = resultSet.getString("image");
            user = new UserLogin(id, username, password, image);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    public UserLogin getByUsernameAndPassword(String username, String password)
    {
        if (username == null)
            throw new IllegalArgumentException("Username must not be null");
        if (password == null)
            throw new IllegalStateException("Password must not be null");
        UserLogin user = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM \"userLogin\" WHERE username = ? AND password = ?");
            statement.setString(1, username);
            statement.setString(2, Objects.toString(Objects.hash(password)));
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            Long id = resultSet.getLong("idUser");
            String image = resultSet.getString("image");
            user = new UserLogin(id, username, password, image);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    public UserLogin getByUsername(String username){
        if (username == null)
            throw new IllegalArgumentException("Username must not be null");
        UserLogin user = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM \"userLogin\" WHERE username = ?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            Long id = resultSet.getLong("idUser");
            String password = resultSet.getString("password");
            String image = resultSet.getString("image");
            user = new UserLogin(id, username, password, image);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    @Override
    public Iterable<UserLogin> getAll() {
        List<UserLogin> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM \"userLogin\"");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                Long id = resultSet.getLong("idUser");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                String image = resultSet.getString("image");

                users.add(new UserLogin(id, username, password, image));
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return users;
    }

    @Override
    public UserLogin add(UserLogin user) {
        if (user == null)
            throw new IllegalArgumentException("User must not be null");
        if (user.getId() == null)
            user.setId(getFirstUnusedId());
        if (getByUsername(user.getUsername()) != null)
            return user;

        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO \"userLogin\" (\"idUser\", username, password, image) VALUES (?, ?, ?, ?)");
            statement.setLong(1, user.getId());
            statement.setString(2, user.getUsername());
            statement.setString(3, String.valueOf(Objects.hash(user.getPassword())));
            statement.setString(4, user.getImage());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    @Override
    public UserLogin remove(Long aLong) {
        return null;
    }

    @Override
    public UserLogin update(UserLogin entity) {
        return null;
    }
}
