package com.network.laborator6.domain;

import com.network.laborator6.utils.Constants;

import java.time.LocalDateTime;

public class Message extends Entity<Long>{
    private Long idFrom;
    private Long idTo;
    private String message;
    private LocalDateTime dateTime;
    private Long reply;

    public Message(Long id, Long idFrom, Long idTo, String message, LocalDateTime dateTime, Long reply) {
        this.setId(id);
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.message = message;
        this.dateTime = dateTime;
        this.reply = reply;
    }

    public Message(Long idFrom, Long idTo, String message, LocalDateTime dateTime, Long reply) {
        this.setId(null);
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.message = message;
        this.dateTime = dateTime;
        this.reply = reply;
    }

    public Message(Long idFrom, Long idTo, String message, Long reply) {
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.message = message;
        this.dateTime = LocalDateTime.now();
        this.reply = reply;
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Long getReply() {
        return reply;
    }

    public void setReply(Long reply) {
        this.reply = reply;
    }

    public Long getIdTo() {
        return idTo;
    }

    public void setIdTo(Long idTo) {
        this.idTo = idTo;
    }

    @Override
    public String toString() {
        return "\nId: " + id + " || From: " + idFrom + " || Reply: " + reply +
                "\n" + dateTime.format(Constants.DATE_TIME_FORMATTER) + " || Message: " + message;
    }
}
