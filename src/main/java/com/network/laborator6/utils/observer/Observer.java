package com.network.laborator6.utils.observer;

import com.network.laborator6.utils.events.Event;

public interface Observer<E extends Event> {
    void update(E e);
}