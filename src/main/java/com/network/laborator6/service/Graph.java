package com.network.laborator6.service;

import com.network.laborator6.domain.Friendship;
import com.network.laborator6.domain.User;

import java.util.*;

public class Graph {
    private final Map<Long, List<Long>> adjacencyList;
    private final Map<Long, Boolean> isVisited;
    private final List<List<Long>> communities;
    private int numberCommunities;

    /**
     * Constructor
     */
    public Graph() {
        adjacencyList = new HashMap<>();
        communities = new ArrayList<>();
        isVisited = new HashMap<>();
        numberCommunities = 0;
    }

    /**
     * Build an adjacency list
     * @param users - all users
     * @param friendships - all friendships
     */
    public void buildGraph(Iterable<User> users, Iterable<Friendship> friendships){
        users.forEach(user -> {
            List<Long> listNodes = new ArrayList<>();
            adjacencyList.put(user.getId(), listNodes);
            isVisited.put(user.getId(), false);
        });

        friendships.forEach(friendship -> {
            adjacencyList.get(friendship.getFirst()).add(friendship.getSecond());
            adjacencyList.get(friendship.getSecond()).add(friendship.getFirst());
        });
    }

    /**
     * DFS visit
     * @param node - id - Long
     */
    public void dfsVisit(Long node){
        isVisited.put(node, true);

        List<Long> lineNode = adjacencyList.get(node);
        lineNode.forEach(x -> {
            if (!isVisited.get(x)) {
                communities.get(numberCommunities).add(x);
                dfsVisit(x);
            }
        });
    }

    /**
     * DFS
     * Construct the communities
     */
    public void dfs(){
        adjacencyList.forEach((key, value) -> {
            if (!isVisited.get(key)){
                communities.add(new ArrayList<>());
                communities.get(numberCommunities).add(key);
                dfsVisit(key);
                ++numberCommunities;
            }
        });
    }

    /**
     * @return number of communities - Long
     */
    public int getNumberCommunities(){
        dfs();
        return numberCommunities;
    }

    /**
     * @return the biggest community as an ids list
     */
    public List<Long> getBiggestCommunity()
    {
        dfs();
        final List<Long>[] maxList = new List[]{new ArrayList<>()};
        communities.forEach(community -> {
            if (community.size() > maxList[0].size())
                maxList[0] = community;
        });
        return maxList[0];
    }

    public List<List<Long>> getCommunities() {
        dfs();
        return communities;
    }
}
