package com.network.laborator6.exception;

public class EntityNotValid extends RuntimeException {
    public EntityNotValid(String exception) {
        super(exception);
    }
}
