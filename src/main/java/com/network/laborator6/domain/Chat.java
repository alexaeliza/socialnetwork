package com.network.laborator6.domain;

import java.time.LocalDate;
import java.util.List;

public class Chat extends Entity<Long>{
    //chat creation date
    private LocalDate date;
    private List<Long> messages;
    private List<Long> users;

    public Chat(List<Long> messages, List<Long> users) {
        this.setId(null);
        this.date = LocalDate.now();
        this.messages = messages;
        this.users = users;
    }

    public Chat(Long id, LocalDate date, List<Long> messages, List<Long> users) {
        this.setId(id);
        this.date = date;
        this.messages = messages;
        this.users = users;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Long> getMessages() {
        return messages;
    }

    public void setMessages(List<Long> messages) {
        this.messages = messages;
    }

    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        String[] messages = new String[1];
        messages[0] = "";
        this.messages.forEach(idMessage -> {
            messages[0] += idMessage;
            messages[0] += "\n";
        });
        return "idChat = " + getId() + "\n" +
                "dateChat = " + date + "\n" +
                messages[0];
    }
}
