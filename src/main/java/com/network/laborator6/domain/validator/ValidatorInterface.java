package com.network.laborator6.domain.validator;

import com.network.laborator6.exception.EntityNotValid;

public interface ValidatorInterface<E> {
    /**
     *
     * @param entity - the entity to be validated
     * @throws EntityNotValid if the entity is not valid
     */
    void validateEntity(E entity) throws EntityNotValid;
}
