package com.network.laborator6.gui;


import com.network.laborator6.HelloApplication;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {

    private final ServiceUser serviceUser;
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceFriendship serviceFriendship;
    private final ServiceChat serviceChat;

    @FXML
    private Label lblTextError;

    @FXML
    private Button btnLogin;

    @FXML
    private PasswordField txtPasswordHidden;

    @FXML
    private TextField txtPasswordText;

    @FXML
    private TextField txtUsername;

    @FXML
    private ImageView imgHidePassword;

    @FXML
    private ImageView imgShowPassword;

    public LoginController(ServiceUser serviceUser, ServiceFriendRequest serviceFriendRequest, ServiceFriendship serviceFriendship, ServiceChat serviceChat) {
        this.serviceUser = serviceUser;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceFriendship = serviceFriendship;
        this.serviceChat = serviceChat;
    }

    @FXML
    protected void onLoginButtonClick(ActionEvent actionEvent) throws IOException {
        try{
            if (txtPasswordHidden.isVisible())
                serviceUser.findUserLogin(txtUsername.getText(), txtPasswordHidden.getText());
            else
                serviceUser.findUserLogin(txtUsername.getText(), txtPasswordText.getText());
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("mainWindow-view.fxml"));
            Long id = serviceUser.searchUsername(txtUsername.getText()).getId();
            fxmlLoader.setController(new MainWindowController(txtUsername.getText(), id, serviceFriendship, serviceFriendRequest, serviceUser, serviceChat));
            Scene scene = new Scene(fxmlLoader.load(), 780, 503);
            Stage stage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
            stage.setTitle("PFly");
            stage.setScene(scene);
            stage.show();
        } catch (ServiceException ex){
            lblTextError.setText("Your username or password is incorrect");
        }
    }

    @FXML
    protected void onSignInButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("signIn-view.fxml"));
        SignInController signInController = new SignInController(this.serviceUser, serviceFriendship, serviceFriendRequest, serviceChat);
        fxmlLoader.setController(signInController);
        Scene scene = new Scene(fxmlLoader.load(), 481, 562);
        Stage stage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setTitle("Sign in");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void showYourPassword(){
        txtPasswordText.setText(txtPasswordHidden.getText());
        txtPasswordText.setVisible(true);
        txtPasswordHidden.setVisible(false);
        imgHidePassword.setVisible(true);
        imgShowPassword.setVisible(false);
    }

    @FXML
    protected void hideYourPassword(){
        txtPasswordHidden.setText(txtPasswordText.getText());
        txtPasswordText.setVisible(false);
        txtPasswordHidden.setVisible(true);
        imgHidePassword.setVisible(false);
        imgShowPassword.setVisible(true);
    }
}
