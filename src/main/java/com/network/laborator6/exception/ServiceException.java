package com.network.laborator6.exception;

public class ServiceException extends RuntimeException {
    public ServiceException(String exception) {
        super(exception);
    }
}
