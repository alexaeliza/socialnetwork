package com.network.laborator6.domain;

import com.network.laborator6.domain.enums.Gender;

import java.time.LocalDate;

public class User extends Entity<Long> {
    private String firstName;
    private String lastName;
    private String username;
    private LocalDate birthday;
    private Gender gender;
    private String city;
    private String phoneNumber;

    public User(String firstName, String lastName, String username, LocalDate birthday, Gender gender, String phoneNumber, String city) {
        this.setId(null);
        setFirstName(firstName);
        setLastName(lastName);
        setUsername(username);
        setBirthday(birthday);
        setGender(gender);
        setPhoneNumber(phoneNumber);
        setCity(city);
    }

    public User(Long id, String firstName, String lastName, String username, LocalDate birthday, Gender gender, String phoneNumber, String city) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setUsername(username);
        setBirthday(birthday);
        setGender(gender);
        setPhoneNumber(phoneNumber);
        setCity(city);
    }

    public User() {
        this.setId(null);
        firstName = "";
        lastName = "";
        username = "";
        birthday = null;
        gender = Gender.other;
        city = "";
        phoneNumber = "";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "\nID: " + id + "\nFirst Name: " + firstName + "\nLast Name: "
                + lastName + "\nUsername: " + username + "\nBirthday: " + birthday + "\nGender: " + gender.toString() +
                "\nPhoneNumber: " + phoneNumber + "\nCity: " + city + "\n";
    }
}
