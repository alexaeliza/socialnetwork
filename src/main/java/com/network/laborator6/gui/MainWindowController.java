package com.network.laborator6.gui;

import com.network.laborator6.HelloApplication;
import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {
    private final String username;
    private final Long id;
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceUser serviceUser;
    private final ServiceFriendship serviceFriendship;
    private final ServiceChat serviceChat;

    @FXML
    private Tooltip tooltipHomeBtn;

    @FXML
    private BorderPane borderPaneMain;

    @FXML
    private Button btnFriendships;

    @FXML
    private Button btnHome;

    @FXML
    private Button btnFriendRequests;

    @FXML
    private Button btnUpdateProfile;

    @FXML
    private Button btnSendFriendRequest;

    @FXML
    private Button btnLogout;

    public MainWindowController(String username, Long id, ServiceFriendship serviceFriendship, ServiceFriendRequest serviceFriendRequest, ServiceUser serviceUser, ServiceChat serviceChat) {
        this.username = username;
        this.id = id;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceUser = serviceUser;
        this.serviceFriendship = serviceFriendship;
        this.serviceChat = serviceChat;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       try {
            onHomeButtonClick((ActionEvent) btnHome.getOnMouseClicked());
        } catch (IOException e) {
           e.printStackTrace();
       }
    }

    @FXML
    protected void onHomeButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("home-view.fxml"));
        fxmlLoader.setController(new HomeController(this.id, serviceUser, serviceFriendRequest, serviceFriendship, serviceChat));
        Pane view = fxmlLoader.load();
        borderPaneMain.setCenter(view);
    }

    @FXML
    protected void onFriendshipsButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("friendships-view.fxml"));
        FriendshipsController friendshipsController = new FriendshipsController(this.serviceFriendship, this.serviceUser, id, username);
        fxmlLoader.setController(friendshipsController);
        Pane view = fxmlLoader.load();
        borderPaneMain.setCenter(view);
    }

    @FXML
    protected void onFriendRequestsButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("friendRequests-view.fxml"));
        fxmlLoader.setController(new FriendRequestsController(serviceFriendRequest, serviceUser, id, username));
        Pane view = fxmlLoader.load();
        borderPaneMain.setCenter(view);
    }

    @FXML void onSendFriendRequestButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("editFriendRequest-view.fxml"));
        fxmlLoader.setController(new EditFriendRequestController(id, username, serviceFriendRequest, serviceUser));
        Pane view = fxmlLoader.load();
        borderPaneMain.setCenter(view);
    }

    @FXML
    protected void onLogoutButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login-view.fxml"));
        LoginController loginController = new LoginController(serviceUser, serviceFriendRequest, serviceFriendship, serviceChat);
        fxmlLoader.setController(loginController);
        Scene scene = new Scene(fxmlLoader.load(), 588, 370);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setTitle("Log in");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void onUpdateProfileButtonClick(ActionEvent actionEvent) {

    }
}
