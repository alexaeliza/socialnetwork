package com.network.laborator6.domain;

import java.io.Serializable;
import java.util.Objects;

public class Pair<VALUE1, VALUE2> implements Serializable {
    private final VALUE1 value1;
    private final VALUE2 value2;

    public Pair(VALUE1 value1, VALUE2 value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public VALUE1 getFirst() {
        return value1;
    }

    public VALUE2 getSecond() {
        return value2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return (Objects.equals(value1, pair.value1) && Objects.equals(value2, pair.value2));
    }

    @Override
    public int hashCode() {
        return Objects.hash(value1, value2);
    }

    @Override
    public String toString() {
        return "<" + value1 +
                ", " + value2 + ">";
    }
}
