package com.network.laborator6.repository.database;

import com.network.laborator6.domain.Chat;
import com.network.laborator6.repository.RepositoryEntity;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RepositoryChatDatabase implements RepositoryEntity<Long, Chat> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryChatDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * @return: the id next to the biggest id in the chat table
     */
    public Long getTheNextId(){
        String sql = "SELECT chat.id FROM chat ORDER BY chat.id DESC";
        Long idChat = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            idChat = resultSet.getLong("id");
            idChat += 1;
        }
        catch (SQLException sqlException) {
            return 1L;
        }
        return idChat;
    }

    /**
     * Search chat with id idChat
     * @param idChat: Long
     * @return: Chat if it exists, otherwise null
     */
    @Override
    public Chat getById(Long idChat) {
        if (idChat == null)
            throw new IllegalArgumentException("Id must not be null");
        Chat chat = null;
        String sql = "SELECT * FROM chat WHERE chat.id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, idChat);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            LocalDate date = resultSet.getDate("dateCreation").toLocalDate();
            List<Long> messages = getIdMessagesFromChat(idChat);
            List<Long> users = getIdUsersFromChat(idChat);
            chat = new Chat(idChat, date, messages, users);

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return chat;
    }

    /**
     * @return: a list with all chats
     */
    @Override
    public Iterable<Chat> getAll() {
        List<Chat> chats = new ArrayList<>();
        String sql =  "SELECT * FROM public.chat";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                LocalDate date = resultSet.getDate("dateCreation").toLocalDate();
                List<Long> messages = getIdMessagesFromChat(id);
                List<Long> users = getIdUsersFromChat(id);
                Chat chat = new Chat(id, date, messages, users);
                chats.add(chat);
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return chats;
    }

    /**
     * Add a new chat if it doesn't exist
     * @param chat: Chat
     * @return: null if the addition was successful,
     *          chat if it exists
     */
    @Override
    public Chat add(Chat chat) {
        if (chat == null)
            throw new IllegalArgumentException("Chat must not be null");
        if (getById(chat.getId()) != null)
            return chat;

        String sql = "INSERT INTO chat (id, \"dateCreation\")  VALUES (?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, chat.getId());
            statement.setDate(2, Date.valueOf(chat.getDate()));
            statement.execute();
            insertInUserChat(chat.getId(), chat.getUsers());
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    @Override
    public Chat remove(Long aLong) {
        return null;
    }

    @Override
    public Chat update(Chat entity) {
        return null;
    }

    /**
     *
     * @param idChat: Long
     * @return: a sorted list of message ids in a specific chat
     */
    private List<Long> getIdMessagesFromChat(Long idChat) {
        List<Long> idMessages = new ArrayList<>();
        String sql = "SELECT id FROM message WHERE message.\"toIdChat\" = ?";
        return getIdsList(idChat, idMessages, sql);
    }

    /**
     *
     * @param idChat: Long
     * @return: a sorted list of user ids in a specific chat
     */
    private List<Long> getIdUsersFromChat(Long idChat){
        List<Long> idUsers = new ArrayList<>();
        String sql = "SELECT \"user\".id FROM \"user\"" +
        "INNER JOIN user_chat uc ON \"user\".id = uc.\"idUser\"" +
        "INNER JOIN chat c ON c.id = uc.\"idChat\"" +
        "WHERE c.id = ?";
        return getIdsList(idChat, idUsers, sql);
    }

    /**
     *
     * @param idUser: Long
     * @return: a sorted list of chat ids for a specific user
     */
    public List<Long> getIdChatsForAUser(Long idUser){
        List<Long> idChats = new ArrayList<>();
        String sql = "SELECT chat.id FROM chat " +
                "INNER JOIN user_chat uc ON chat.id = uc.\"idChat\" " +
                "INNER JOIN \"user\" u ON u.id = uc.\"idUser\" " +
                "WHERE u.id = ?";
        return getIdsList(idUser, idChats, sql);
    }

    /**
     *
     * @param idSearch: Long
     * @param idsList: List of Long
     * @param sql: String, sql command
     * @return: a list of all ids that have idSearch in common
     */
    private List<Long> getIdsList(Long idSearch, List<Long> idsList, String sql) {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, idSearch);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                idsList.add(id);
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        Collections.sort(idsList);
        return idsList;
    }

    /**
     * Inserts connections between a chat and a list of users
     * @param idChat: Long
     * @param idUsers: List of user ids
     */
    private void insertInUserChat(Long idChat, List<Long> idUsers){
        String sql = "INSERT INTO user_chat (\"idUser\", \"idChat\") VALUES (?, ?)";
        idUsers.forEach(idUser -> {
            try (Connection connection = DriverManager.getConnection(url, username, password)) {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setLong(1, idUser);
                statement.setLong(2, idChat);
                statement.execute();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.getMessage());
                System.exit(0);
            }
        });
    }
}
