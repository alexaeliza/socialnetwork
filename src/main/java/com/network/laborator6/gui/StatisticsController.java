package com.network.laborator6.gui;

import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.*;

public class StatisticsController {
    private final ServiceChat serviceChat;
    private final ServiceFriendship serviceFriendship;
    private final ServiceFriendRequest serviceFriendRequest;
    private final Long id;
    private final String username;

    @FXML
    private DatePicker startDate;

    @FXML
    private DatePicker endDate;

    @FXML
    private Button friendshipsStatistics;

    @FXML
    private Button messagesStatistics;

    public StatisticsController(ServiceChat serviceChat, ServiceFriendship serviceFriendship, ServiceFriendRequest serviceFriendRequest, Long id, String username) {
        this.serviceChat = serviceChat;
        this.serviceFriendship = serviceFriendship;
        this.serviceFriendRequest = serviceFriendRequest;
        this.id = id;
        this.username = username;
    }

    public boolean validateDate() {
        if (startDate.getValue() != null && endDate.getValue() != null) {
            if (startDate.getValue().isAfter(endDate.getValue())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("The start date must be before end date\nPlase modify before trying again");
                alert.setTitle("Date error");
                alert.showAndWait();
                return false;
            }
            return true;
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("The start date and the end date must not be empty\nPlase modify before trying again");
            alert.setTitle("Date error");
            alert.showAndWait();
        }
        return false;
    }

    public void handleFriendships(ActionEvent actionEvent) {
        if (validateDate()) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File("src/main/resources"));
            Stage stage = new Stage();
            File selectedDirectory = directoryChooser.showDialog(stage);
            File file = new File(selectedDirectory.getAbsolutePath() + "/" + "friendUser" + id + ".txt");
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                int received = serviceFriendRequest.getNumberReceivedFriendRequests(id, startDate.getValue(), endDate.getValue());
                int sent = serviceFriendRequest.getNumberSentFriendRequests(id, startDate.getValue(), endDate.getValue());
                int friendships = serviceFriendship.getNumberFriendships(id, startDate.getValue(), endDate.getValue());
                bufferedWriter.write("Between " + startDate.getValue().toString() + " and " + endDate.getValue() + ":\n");
                bufferedWriter.write("You received " + received + " new friend requests\n");
                bufferedWriter.write("You sent " + sent + " new friend requests\n");
                bufferedWriter.write("You became friend with " + friendships + " users\n");
                bufferedWriter.close();
            } catch (IOException ioException) {
                System.out.println(ioException.getMessage());
            }
        }
    }

    public void handleMessages(ActionEvent actionEvent) {
        if (validateDate()) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setInitialDirectory(new File("src/main/resources"));
            Stage stage = new Stage();
            File selectedDirectory = directoryChooser.showDialog(stage);
            File file = new File(selectedDirectory.getAbsolutePath() + "/" + "messageUser" + id + ".txt");
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                int received = serviceChat.getNumberReceivedMessages(id, startDate.getValue(), endDate.getValue());
                int sent = serviceChat.getNumberSentMessages(id, startDate.getValue(), endDate.getValue());
                bufferedWriter.write("Between " + startDate.getValue().toString() + " and " + endDate.getValue() + ":\n");
                bufferedWriter.write("You received " + received + " new messages\n");
                bufferedWriter.write("You sent " + sent + " new messages\n");
                bufferedWriter.close();
            } catch (IOException ioException) {
                System.out.println(ioException.getMessage());
            }
        }
    }
}
