package com.network.laborator6.domain.validator;

import com.network.laborator6.domain.enums.Gender;
import com.network.laborator6.exception.EntityNotValid;
import com.network.laborator6.domain.User;
import com.network.laborator6.utils.Constants;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class UserValidator implements ValidatorInterface<User> {
    private String errors;

    public UserValidator() {
        errors = "";
    }

    /**
     * Check if a string contains a number
     * @param string: String
     * @return: true if contains a number, otherwise false
     */
    private boolean containsDigit(String string) {
        for (int i = 0; i < string.length(); i++)
            if (string.charAt(i) >= '0' && string.charAt(i) <= '9')
                return true;
        return false;
    }

    /**
     * Check that a string contains only numbers
     * @param string: String
     * @return: true if string contains only numbers, otherwise false
     */
    private boolean isNumber(String string) {
        for (int i = 0; i < string.length(); i++)
            if (string.charAt(i) < '0' || string.charAt(i) > '9')
                return false;
        return true;
    }

    /**
     * Check if a string can be a gender for the entity
     * Gender must be female/ male/ other
     * @param gender: String
     */
    public void validGender(String gender) {
        if (gender == null)
            throw new EntityNotValid("Gender must be female/ male/ other\n");
        try {
            Gender.valueOf(gender);
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new EntityNotValid("Gender must be female/ male/ other\n");
        }
    }

    public void validPassword(String password){
        if (Objects.equals(password, ""))
            throw new EntityNotValid("Password can not be empty\n");
    }

    /**
     * Check if a string is a valid date
     * @param birthday: String
     */
    public void validBirthday(String birthday) {
        errors = "";
        if (birthday.isEmpty())
            errors = errors + "Birthday can not be empty\n";
        else
            try {
                LocalDate.parse(birthday, Constants.DATE_FORMATTER);
            } catch (DateTimeParseException exception) {
                errors = errors + "Date is not in the yyyy-mm-dd format\n";
            }
        if (!errors.isEmpty())
            throw new EntityNotValid(errors);
    }

    /**
     * Check that the user entity is valid
     * @param user: user
     */
    public void validateEntity(User user) {
        errors = "";
        if (user.getFirstName().isEmpty())
            errors = errors + "First Name can not be empty\n";
        if (containsDigit(user.getFirstName()))
            errors = errors + "First Name can not contain digits\n";
        if (user.getFirstName().contains(","))
            errors = errors + "First Name can not contain ,\n";
        if (user.getLastName().isEmpty())
            errors = errors + "Last Name can not be empty\n";
        if (containsDigit(user.getLastName()))
            errors = errors + "Last Name can not contain digits\n";
        if (user.getLastName().contains(","))
            errors = errors + "Last Name can not contain ,\n";
        if (user.getUsername().isEmpty())
            errors = errors + "Username can not be empty\n";
        if (user.getUsername().contains(","))
            errors = errors + "Username can not contain ,\n";
        if (user.getPhoneNumber().isEmpty())
            errors = errors + "Phone number can not be empty\n";
        if (!isNumber(user.getPhoneNumber()))
            errors = errors + "Phone number can not contain other symbols than digits\n";
        if (user.getCity().isEmpty())
            errors = errors + "City can not be empty\n";
        if (containsDigit(user.getCity()))
            errors = errors + "City can not contain digits\n";
        if (!errors.isEmpty())
            throw new EntityNotValid(errors);
    }
}
