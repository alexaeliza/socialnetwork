package com.network.laborator6.gui;

import com.network.laborator6.HelloApplication;
import com.network.laborator6.exception.EntityNotValid;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.repository.database.RepositoryUserLoginDatabase;
import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import com.network.laborator6.utils.Constants;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;


public class SignInController implements Initializable {

    private final ServiceUser serviceUser;
    private final ServiceFriendship serviceFriendship;
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceChat serviceChat;

    @FXML
    private Button btnBackToLogin;

    @FXML
    private Button btnSignIn;

    @FXML
    private ComboBox<String> comboBoxGender;

    @FXML
    private DatePicker dateBirthDate;

    @FXML
    private TextField txtCity;

    @FXML
    private TextField txtFirstName;

    @FXML
    private TextField txtLastName;

    @FXML
    private PasswordField txtPasswordHidden;

    @FXML
    private TextField txtPhoneNumber;

    @FXML
    private TextField txtUsername;

    @FXML
    private Label lblTextError;

    @FXML
    private ImageView imgHidePassword;

    @FXML
    private TextField txtPasswordVisible;

    @FXML
    private ImageView imgShowPassword;

    @FXML
    private Button btnLoadPicture;

    @FXML
    private TextField txtURLPicture;


    public SignInController(ServiceUser serviceUser, ServiceFriendship serviceFriendship, ServiceFriendRequest serviceFriendRequest, ServiceChat serviceChat) {
        this.serviceUser = serviceUser;
        this.serviceFriendship = serviceFriendship;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceChat = serviceChat;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comboBoxGender.setItems(FXCollections.observableArrayList("female", "male", "other"));
    }

    @FXML
    protected void onBackToLoginButtonClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login-view.fxml"));
        LoginController loginController = new LoginController(serviceUser, serviceFriendRequest, serviceFriendship, serviceChat);
        fxmlLoader.setController(loginController);
        Scene scene = new Scene(fxmlLoader.load(), 588, 370);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setTitle("Log in");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void onSignInButtonClick(ActionEvent actionEvent) throws IOException {

        try {
            String firstName = txtFirstName.getText();
            String lastName = txtLastName.getText();
            String birtDate = "";
            if (dateBirthDate.getValue() != null)
                birtDate = dateBirthDate.getValue().toString();
            String gender = comboBoxGender.getValue();
            String city = txtCity.getText();
            String phoneNumber = txtPhoneNumber.getText();
            String username = txtUsername.getText();
            String password = "";
            if(txtPasswordVisible.isVisible())
                password = txtPasswordVisible.getText();
            else
                password = txtPasswordHidden.getText();
            String image = txtURLPicture.getText();
            if (image.isEmpty())
                image = Constants.PATH_USER_PICTURE_INCOGNITO;
            serviceUser.addUserSignIn(firstName, lastName, username, birtDate, gender, phoneNumber, city, password, image);
            openTheMainWindow(actionEvent);
        } catch (EntityNotValid | ServiceException | NullPointerException ex) {
            lblTextError.setText(ex.getMessage());
        }
    }

    private void openTheMainWindow(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("mainWindow-view.fxml"));
        Long id = serviceUser.searchUsername(txtUsername.getText()).getId();
        fxmlLoader.setController(new MainWindowController(txtUsername.getText(), id, serviceFriendship, serviceFriendRequest, serviceUser, serviceChat));
        Scene scene = new Scene(fxmlLoader.load(), 780, 503);
        Stage stage = (Stage)((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setTitle("PFly");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    protected void showYourPassword(){
        txtPasswordVisible.setText(txtPasswordHidden.getText());
        txtPasswordVisible.setVisible(true);
        txtPasswordHidden.setVisible(false);
        imgHidePassword.setVisible(true);
        imgShowPassword.setVisible(false);

    }

    @FXML
    protected void hideYourPassword(){
        txtPasswordHidden.setText(txtPasswordVisible.getText());
        txtPasswordVisible.setVisible(false);
        txtPasswordHidden.setVisible(true);
        imgHidePassword.setVisible(false);
        imgShowPassword.setVisible(true);
    }

    @FXML
    void singleFileChooser(ActionEvent actionEvent){
        FileChooser fileChooser = new FileChooser();
        //fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPG file", "*.jpg"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG file", "*.png"));

        File file = fileChooser.showOpenDialog(null);

        if (file != null){
            txtURLPicture.setText(file.toString());
        }
    }
}


