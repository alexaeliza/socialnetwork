package com.network.laborator6.domain.enums;

public enum Gender {
    female,
    male,
    other
}
