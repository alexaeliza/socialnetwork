package com.network.laborator6.service;

import com.network.laborator6.domain.Friendship;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.domain.enums.Gender;
import com.network.laborator6.domain.User;
import com.network.laborator6.domain.validator.UserValidator;
import com.network.laborator6.exception.IdNotFound;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.exception.UsernameNotFound;
import com.network.laborator6.repository.database.RepositoryFriendshipDatabase;
import com.network.laborator6.repository.database.RepositoryUserDatabase;
import com.network.laborator6.repository.database.RepositoryUserLoginDatabase;
import com.network.laborator6.utils.Constants;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class ServiceUser {
    public ServiceUser(RepositoryUserDatabase repositoryUsers, RepositoryFriendshipDatabase repositoryFriendships, RepositoryUserLoginDatabase repositoryUserLoginDatabase, UserValidator validatorUser) {
        this.repositoryUsers = repositoryUsers;
        this.repositoryUserLoginDatabase = repositoryUserLoginDatabase;
        this.validatorUser = validatorUser;
    }

    private final RepositoryUserDatabase repositoryUsers;
    private final RepositoryUserLoginDatabase repositoryUserLoginDatabase;
    private final UserValidator validatorUser;

    /**
     *
     * @return a list of all users in the repository
     */
    public Iterable<User> getUsers() {
        return repositoryUsers.getAll();
    }

    /**
     *
     * @param username the username that will be searched in the list
     * @return a list of users that have the username username
     * @throws UsernameNotFound if the username does not exist
     */
    public User searchUsername(String username) {
        AtomicReference<User> validUser = new AtomicReference<>(new User());
        getUsers().forEach(user -> {
            if (user.getUsername().equals(username))
                validUser.set(user);
        });
        return validUser.get();
    }

    /**
     *
     * @param id the id that will be searched in the list
     * @param users the list in which the id is searched
     * @return the user from users that has the id id
     * @throws IdNotFound if there is no user with the id id in users
     */
    public User searchId(Long id, List<User> users) {
        for (User user : users)
            if (user.getId().equals(id))
                return user;
        throw new IdNotFound();
    }

    /**
     *
     * @param firstName the firstname of the user
     * @param lastName the lastname of the user
     * @param userName the username of the user
     * @param birthday the birthday of the user
     * @return the id of the user added
     * @throws ServiceException if the user already exists
     */
    public Long addUser(String firstName, String lastName, String userName, String birthday, String gender, String phoneNumber, String city) {
        validatorUser.validBirthday(birthday);
        validatorUser.validGender(gender);
        User user = new User(firstName, lastName, userName, LocalDate.parse(birthday, Constants.DATE_FORMATTER), Gender.valueOf(gender), phoneNumber, city);
        validatorUser.validateEntity(user);
        if (repositoryUsers.add(user) == user)
            throw new ServiceException("User already exists");
        return user.getId();
    }

    /**
     *
     * @param id the id of the user that will be deleted
     * @throws ServiceException if there is no user with the id id
     */
    public void removeUser(Long id) {
        User user = repositoryUsers.remove(id);
        if (user == null)
            throw new ServiceException("User does not exist");
    }

    /**
     * Find a user by username and password
     * @param username: String
     * @param password: String
     * @return: the user who has the password and the username
     */
    public UserLogin findUserLogin(String username, String password){
        UserLogin user = repositoryUserLoginDatabase.getByUsernameAndPassword(username, password);
        if (user == null)
            throw new ServiceException("This user does not exist");
        return user;
    }

    public void addUserSignIn(String firstName, String lastName, String username, String birthday, String gender, String phoneNumber, String city, String password, String image){
        validatorUser.validPassword(password);
        addUser(firstName, lastName, username, birthday, gender, phoneNumber, city);
        User user = repositoryUsers.getByUsername(username);
        UserLogin userLogin = new UserLogin(user.getId(), username, password, image);
        if(repositoryUserLoginDatabase.add(userLogin) == userLogin)
            throw new ServiceException("User already exists");
    }

    public UserLogin findUserLoginById(Long id){
        UserLogin user = repositoryUserLoginDatabase.getById(id);
        if (user == null)
            throw new ServiceException("This user does not exist");
        return user;
    }
}
