package com.network.laborator6.repository.database;

import com.network.laborator6.domain.Message;
import com.network.laborator6.repository.RepositoryEntity;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RepositoryMessageDatabase implements RepositoryEntity<Long, Message> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryMessageDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * @return: the id next to the biggest id in the message table
     */
    public Long getTheNextId(){
        String sql = "SELECT id FROM message ORDER BY id DESC";
        Long idMessage = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            idMessage = resultSet.getLong("id");
            idMessage += 1;
        }
        catch (SQLException sqlException) {
            return 1L;
        }
        return idMessage;
    }

    /**
     * Search message with id idMessage
     * @param idMessage: Long
     * @return: Message if it exists, otherwise null
     */
    @Override
    public Message getById(Long idMessage) {
        if (idMessage == null)
            throw new IllegalArgumentException("Id must not be null");
        String sql = "SELECT * FROM message WHERE message.id = ?";
        Message message = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, idMessage);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            Long fromIdUser = resultSet.getLong("fromIdUser");
            Long toIdChat = resultSet.getLong("toIdChat");
            String messageStr = resultSet.getString("message");
            LocalDateTime dateTime = resultSet.getTimestamp("date").toLocalDateTime();
            Long reply = resultSet.getLong("reply");
            message = new Message(idMessage, fromIdUser, toIdChat, messageStr, dateTime, reply);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return message;
    }

    /**
     * @return: a list with all messages
     */
    @Override
    public Iterable<Message> getAll() {
        List<Message> messages = new ArrayList<>();
        String sql = "SELECT * FROM public.message";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            getAMessageList(messages, resultSet);

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return messages;
    }

    /**
     * Build a list of messages resulting from the execution of the sql command
     * @param messages: List of messages
     * @param resultSet: the result of executing the sql command
     */
    private void getAMessageList(List<Message> messages, ResultSet resultSet) throws SQLException {
        while(resultSet.next()) {
            Long id = resultSet.getLong("id");
            Long fromIdUser = resultSet.getLong("fromIdUser");
            Long toIdChat = resultSet.getLong("toIdChat");
            String message = resultSet.getString("message");
            LocalDateTime dateTime = resultSet.getTimestamp("date").toLocalDateTime();
            Long reply = resultSet.getLong("reply");

            Message newMessage = new Message(id, fromIdUser, toIdChat, message, dateTime, reply);
            messages.add(newMessage);
        }
    }

    /**
     * Add a new message if it doesn't exist
     * @param message: Chat
     * @return: null if the addition was successful,
     *          message if it exists
     */
    @Override
    public Message add(Message message) {
        if (message == null)
            throw new IllegalArgumentException("Message must not be null");
        if (getById(message.getId()) != null)
            return message;

        String sql = "INSERT INTO message (id, \"fromIdUser\", \"toIdChat\", message, date, reply) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, message.getId());
            statement.setLong(2, message.getIdFrom());
            statement.setLong(3, message.getIdTo());
            statement.setString(4, message.getMessage());
            statement.setTimestamp(5, Timestamp.valueOf(message.getDateTime()));
            if(message.getReply() != null)
                statement.setLong(6, message.getReply());
            else
                statement.setLong(6, 0);
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    @Override
    public Message remove(Long aLong) {
        return null;
    }

    /**
     *
     * @param: message the entity that will be updated
     * @return: message if it doesn't exist
     *          null if the change was successful
     */
    @Override
    public Message update(Message message) {
        if (message == null)
            throw new IllegalArgumentException("Message must not be null");
        if (getById(message.getId()) == null)
            return message;

        String sql = "UPDATE message\n" +
                "SET \"fromIdUser\" = ?, \"toIdChat\" = ?, message = ?, date = ?, reply = ?\n" +
                "WHERE id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, message.getIdFrom());
            statement.setLong(2, message.getIdTo());
            statement.setString(3, message.getMessage());
            statement.setTimestamp(4, Timestamp.valueOf(message.getDateTime()));
            statement.setLong(5, message.getReply());
            statement.setLong(6, message.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    /**
     * @param idUser: Long
     * @return: a list of all no reply messages from a user
     */
    public List<Message> getNoReplyMessagesForAUser(Long idUser){
        String sql = "SELECT * FROM message\n" +
                "INNER JOIN chat c ON c.id = message.\"toIdChat\"\n" +
                "INNER JOIN user_chat uc ON c.id = uc.\"idChat\"\n" +
                "INNER JOIN \"user\" u ON u.id = uc.\"idUser\"\n" +
                "WHERE u.id = ? AND message.\"fromIdUser\" != ? AND message.reply = 0";
        List<Message> messages = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, idUser);
            statement.setLong(2, idUser);
            ResultSet resultSet = statement.executeQuery();

            getAMessageList(messages, resultSet);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return messages;
    }
}
