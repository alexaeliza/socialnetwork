package com.network.laborator6.utils;

import java.time.format.DateTimeFormatter;

public class Constants {
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    public static final String URL_POSTGRES = "jdbc:postgresql://localhost:5432/SocialNetworkLab5MAP";
    public static final String USERNAME_POSTGRES = "postgres";
    public static final String PASSWORD_POSTGRES = "postgres";
    public static final String PATH_USER_PICTURE_INCOGNITO = "D:\\facultate\\MAP - Metode avansate de programare\\Laboratoare\\Lab6 part 2\\src\\main\\resources\\images\\anonymous.png";
}
