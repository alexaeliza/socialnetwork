package com.network.laborator6.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Friendship extends Entity<Pair<Long, Long>> {
    private final LocalDate date;

    public Friendship(User friend1, User friend2) {
        Pair<Long, Long> id = new Pair<>(friend1.getId(), friend2.getId());
        this.setId(id);
        date = LocalDate.now();
    }

    public Friendship(Long id1, Long id2, LocalDate date) {
        Pair<Long, Long> id = new Pair<>(id1, id2);
        this.setId(id);
        this.date = date;
    }

    public Friendship(Long id1, Long id2) {
        Pair<Long, Long> id = new Pair<>(id1, id2);
        this.setId(id);
        this.date = LocalDate.now();
    }

    public Long getFirst() {
        return getId().getFirst();
    }

    public Long getSecond() {
        return getId().getSecond();
    }

    public LocalDate getFriendshipDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return (Objects.equals(super.getId().getFirst(), that.getId().getFirst()) && Objects.equals(super.getId().getSecond(), that.getId().getSecond()))
                || (Objects.equals(super.getId().getFirst(), that.getId().getSecond()) && Objects.equals(super.getId().getSecond(), that.getId().getFirst()));
    }

    @Override
    public String toString() {
        return "Id: " + id +
                "\nDate: " + date;
    }
}
