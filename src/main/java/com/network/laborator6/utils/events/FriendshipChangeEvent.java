package com.network.laborator6.utils.events;

import com.network.laborator6.domain.Friendship;

public class FriendshipChangeEvent implements Event{
    private final EventType eventType;
    private final Friendship newFriendship;
    private Friendship oldFriendship;

    public FriendshipChangeEvent(EventType eventType, Friendship newFriendship) {
        this.eventType = eventType;
        this.newFriendship = newFriendship;
    }

    public FriendshipChangeEvent(EventType eventType, Friendship newFriendship, Friendship oldFriendship) {
        this.eventType = eventType;
        this.newFriendship = newFriendship;
        this.oldFriendship = oldFriendship;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Friendship getNewFriendship() {
        return newFriendship;
    }

    public Friendship getOldFriendship() {
        return oldFriendship;
    }
}
