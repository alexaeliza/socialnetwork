package com.network.laborator6.utils.events;

import com.network.laborator6.domain.User;

public class UserChangeEvent implements Event {
    private final EventType eventType;
    private final User newUser;
    private User oldUser;

    public UserChangeEvent(EventType eventType, User newUser) {
        this.eventType = eventType;
        this.newUser = newUser;
    }

    public UserChangeEvent(EventType eventType, User newUser, User oldUser) {
        this.eventType = eventType;
        this.newUser = newUser;
        this.oldUser = oldUser;
    }

    public EventType getEventType() {
        return eventType;
    }

    public User getNewFriendRequest() {
        return newUser;
    }

    public User getOldFriendRequest() {
        return oldUser;
    }
}
