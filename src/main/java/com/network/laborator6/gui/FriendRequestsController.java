package com.network.laborator6.gui;

import com.network.laborator6.HelloApplication;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.domain.enums.Status;
import com.network.laborator6.gui.domainFXML.FriendFXML;
import com.network.laborator6.domain.FriendRequest;
import com.network.laborator6.domain.Pair;
import com.network.laborator6.domain.User;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceUser;
import com.network.laborator6.utils.Constants;
import com.network.laborator6.utils.events.FriendRequestChangeEvent;
import com.network.laborator6.utils.observer.Observer;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class FriendRequestsController implements Observer<FriendRequestChangeEvent>, Initializable{
    @FXML
    TableView<FriendFXML> tableFriendRequests;

    @FXML
    TableColumn<FriendFXML, Circle> columnPicture;

    @FXML
    TableColumn<FriendFXML, String> columnUsername;

    @FXML
    TableColumn<FriendFXML, String> columnFirstName;

    @FXML
    TableColumn<FriendFXML, String> columnLastName;

    @FXML
    TableColumn<FriendFXML, String> columnDate;

    @FXML
    TableColumn<FriendFXML, String> columnStatus;

    @FXML
    TableColumn<FriendFXML, Button> columnAccept;

    @FXML
    TableColumn<FriendFXML, Button> columnReject;

    @FXML
    TableView<FriendFXML> sentFriendRequests;

    @FXML
    TableColumn<FriendFXML, Circle> sentPicture;

    @FXML
    TableColumn<FriendFXML, String> sentUsername;

    @FXML
    TableColumn<FriendFXML, String> sentFirstName;

    @FXML
    TableColumn<FriendFXML, String> sentLastName;

    @FXML
    TableColumn<FriendFXML, String> sentDate;

    @FXML
    TableColumn<FriendFXML, String> sentStatus;

    @FXML
    TableColumn<FriendFXML, Button> sentUnsend;

    private final ObservableList<FriendFXML> friendRequests = FXCollections.observableArrayList();
    private final ObservableList<FriendFXML> sent = FXCollections.observableArrayList();
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceUser serviceUser;
    private final Long id;
    private final String username;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        columnPicture.setCellValueFactory(new PropertyValueFactory<>("picture"));
        columnAccept.setCellValueFactory(new PropertyValueFactory<>("accept"));
        columnReject.setCellValueFactory(new PropertyValueFactory<>("reject"));
        sentPicture.setCellValueFactory(new PropertyValueFactory<>("picture"));
        sentUnsend.setCellValueFactory(new PropertyValueFactory<>("send"));
        columnUsername.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getUsername()));
        columnFirstName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getFirstName()));
        columnLastName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getLastName()));
        columnDate.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getDate().format(Constants.DATE_FORMATTER)));
        columnStatus.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getStatus().toString()));
        sentUsername.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getUsername()));
        sentFirstName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getFirstName()));
        sentLastName.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getLastName()));
        sentDate.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getDate().format(Constants.DATE_FORMATTER)));
        sentStatus.setCellValueFactory(value -> new SimpleStringProperty(value.getValue().getStatus().toString()));
        tableFriendRequests.setItems(friendRequests);
        sentFriendRequests.setItems(sent);
        tableFriendRequests.setPlaceholder(new Label("No available friend requests"));
        sentFriendRequests.setPlaceholder(new Label("No sent friend requests"));
    }

    public FriendRequestsController(ServiceFriendRequest serviceFriendRequest, ServiceUser serviceUser, Long id, String username) throws MalformedURLException {
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceUser = serviceUser;
        this.id = id;
        this.username = username;
        serviceFriendRequest.addObserver(this);
        init();
    }

    private void init() {
        List<FriendRequest> friendRequestList = serviceFriendRequest.getAvailableFriendRequestsForUser(id).values().stream().toList();
        List<FriendFXML> users = new ArrayList<>();
        List<User> allUsers = (List<User>) serviceUser.getUsers();
        friendRequestList.forEach(
                friendRequest -> {
                    User user = serviceUser.searchId(friendRequest.getId().getFirst(), allUsers);
                    FriendFXML friendFXML = new FriendFXML(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(), friendRequest.getDate(), friendRequest.getStatus());
                    Button accept = getButtonAccept(user.getId());
                    Button reject = getButtonReject(user.getId());
                    Circle picture = getPicture(user.getId());
                    friendFXML.setPicture(picture);
                    friendFXML.setReject(reject);
                    friendFXML.setAccept(accept);
                    users.add(friendFXML);
                }
        );
        friendRequests.setAll(users);

        friendRequestList = serviceFriendRequest.getSentFriendRequestsForUser(id).values().stream().toList();
        List<FriendFXML> usersSent = new ArrayList<>();
        friendRequestList.forEach(
                friendRequest -> {
                    User user = serviceUser.searchId(friendRequest.getId().getSecond(), allUsers);
                    FriendFXML friendFXML = new FriendFXML(user.getId(), user.getUsername(), user.getFirstName(), user.getLastName(), friendRequest.getDate(), friendRequest.getStatus());
                    Button unsend = getButtonUnsend(user.getId());
                    Circle picture = getPicture(user.getId());
                    if (!friendFXML.getStatus().equals(Status.rejected))
                        friendFXML.setSend(unsend);
                    friendFXML.setPicture(picture);
                    usersSent.add(friendFXML);
                }
        );
        sent.setAll(usersSent);
    }

    public Button getButtonUnsend(Long idUser) {
        Button button = new Button("");
        button.setId("btnUnsend");
        button.setOnAction(event -> serviceFriendRequest.removeFriendRequest(new Pair<>(id, idUser)));
        return button;
    }

    public Button getButtonAccept(Long idUser) {
        Button button = new Button("");
        button.setId("btnAccept");
        button.setOnAction(event -> serviceFriendRequest.manageFriendRequest(id, idUser, "accept"));
        return button;
    }

    public Button getButtonReject(Long idUser) {
        Button button = new Button("");
        button.setId("btnReject");
        button.setOnAction(event -> serviceFriendRequest.manageFriendRequest(id, idUser, "delete"));
        return button;
    }

    public Circle getPicture(Long id){
        Circle circlePicture = new Circle();
        circlePicture.setRadius(20);
        UserLogin userLogin = serviceUser.findUserLoginById(id);
        File file = new File(userLogin.getImage());
        try {
            Image imgUser = new Image(file.toURI().toURL().toExternalForm());
            circlePicture.setFill(new ImagePattern(imgUser));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return circlePicture;
    }

    @Override
    public void update(FriendRequestChangeEvent friendRequestChangeEvent) {
        init();
    }
}
