package com.network.laborator6.exception;

public class IdNotFound extends RuntimeException {
    public IdNotFound() {
        super("\nID does not exist\n");
    }
}
