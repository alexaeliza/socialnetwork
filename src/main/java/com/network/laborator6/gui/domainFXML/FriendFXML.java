package com.network.laborator6.gui.domainFXML;

import com.network.laborator6.domain.Entity;
import com.network.laborator6.domain.enums.Status;
import javafx.scene.control.Button;
import javafx.scene.shape.Circle;

import java.time.LocalDate;

public class FriendFXML extends Entity<Long> {
    private Circle picture;
    private String username;
    private String firstName;
    private String lastName;
    private LocalDate date;
    private Status status;
    private Button accept;
    private Button reject;
    private Button send;

    public FriendFXML(Long id, String username, String firstName, String lastName, LocalDate date, Status status) {
        this.setId(id);
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.status = status;
    }

    public FriendFXML(Long id, String username, String firstName, String lastName) {
        this.setId(id);
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public FriendFXML() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Circle getPicture() {
        return picture;
    }

    public void setPicture(Circle picture) {
        this.picture = picture;
    }

    public Button getAccept() {
        return accept;
    }

    public void setAccept(Button send) {
        this.accept = send;
    }

    public Button getReject() {
        return reject;
    }

    public void setReject(Button reject) {
        this.reject = reject;
    }

    public Button getSend() {
        return send;
    }

    public void setSend(Button send) {
        this.send = send;
    }
}
