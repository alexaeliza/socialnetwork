package com.network.laborator6.service;

import com.network.laborator6.domain.*;
import com.network.laborator6.domain.enums.Status;
import com.network.laborator6.exception.EntityNotValid;
import com.network.laborator6.exception.IdNotFound;
import com.network.laborator6.exception.ServiceException;
import com.network.laborator6.repository.database.RepositoryFriendRequestDatabase;
import com.network.laborator6.repository.database.RepositoryFriendshipDatabase;
import com.network.laborator6.repository.database.RepositoryUserDatabase;
import com.network.laborator6.utils.events.EventType;
import com.network.laborator6.utils.events.FriendRequestChangeEvent;
import com.network.laborator6.utils.observer.Observable;
import com.network.laborator6.utils.observer.Observer;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ServiceFriendRequest implements Observable<FriendRequestChangeEvent> {
    private final RepositoryFriendRequestDatabase repositoryFriendRequest;
    private final RepositoryUserDatabase repositoryUser;
    private final RepositoryFriendshipDatabase repositoryFriendship;
    private List<Observer<FriendRequestChangeEvent>> observers;

    public ServiceFriendRequest(RepositoryFriendRequestDatabase repositoryFriendRequest, RepositoryUserDatabase repositoryUser, RepositoryFriendshipDatabase repositoryFriendship) {
        this.repositoryFriendRequest = repositoryFriendRequest;
        this.repositoryUser = repositoryUser;
        this.repositoryFriendship = repositoryFriendship;
        observers = new ArrayList<>();
    }

    /**
     *
     * @param id - the id of the User for which received FriendRequests will be searched
     * @return a Map containing all the received FriendRequests
     */
    public Map<User, FriendRequest> getReceivedFriendRequestsForUser(Long id) {
        Iterable<FriendRequest> friendRequests = repositoryFriendRequest.getAll();
        Map<User, FriendRequest> userFriendRequestMap = new HashMap<>();
        friendRequests.forEach(friendRequest -> {
            if (friendRequest.getId().getSecond().equals(id))
                userFriendRequestMap.put(repositoryUser.getById(friendRequest.getId().getFirst()), friendRequest);
        });
        return userFriendRequestMap;
    }

    /**
     *
     * @param id - the id of the User for which available FriendRequests will be searched
     * @return a Map containing all the available FriendRequests
     */
    public Map<User, FriendRequest> getAvailableFriendRequestsForUser(Long id) {
        Iterable<FriendRequest> friendRequests = repositoryFriendRequest.getAll();
        Map<User, FriendRequest> userFriendRequestMap = new HashMap<>();
        friendRequests.forEach(friendRequest -> {
            if (friendRequest.getId().getSecond().equals(id))
                if (friendRequest.getStatus().equals(Status.pending))
                    userFriendRequestMap.put(repositoryUser.getById(friendRequest.getId().getFirst()), friendRequest);
        });
        return userFriendRequestMap;
    }

    /**
     *
     * @param userId - the id of the User that sends the FriendRequest
     * @param friendId - the id of the User that receives the FriendRequest
     * @return the new FriendRequest
     * @throws ServiceException if userId = friendId
     */
    public FriendRequest sendFriendRequest(Long userId, Long friendId) {
        if (userId.equals(friendId))
            throw new ServiceException("\nYou can not send a friend request to yourself\n");

        if (repositoryUser.getById(friendId) == null)
            throw new ServiceException("\nTheree is no user with this id\n");

        FriendRequest friendRequest = new FriendRequest(userId, friendId);
        FriendRequest friendRequest1 = repositoryFriendRequest.getById(new Pair<>(friendId, userId));
        if (friendRequest1 != null && friendRequest1.getStatus().equals(Status.pending)) {
            repositoryFriendship.add(new Friendship(userId, friendId));
            friendRequest1.setStatus(Status.approved);
            repositoryFriendRequest.update(friendRequest1);
            return friendRequest;
        }
        friendRequest1 = repositoryFriendRequest.getById(friendRequest.getId());
        if (friendRequest1 != null) {
            if (friendRequest1.getStatus().equals(Status.pending))
                throw new EntityNotValid("\nThere is already a friend request sent to this user\n");
            if (friendRequest1.getStatus().equals(Status.rejected))
                throw new EntityNotValid("\nThe user rejected your friend request, you can not send another one\n");
            if (friendRequest1.getStatus().equals(Status.approved))
                throw new EntityNotValid("\nYou are already friend with this user\n");
        }
        FriendRequest friendRequest2 = repositoryFriendRequest.add(friendRequest);
        if (friendRequest2 == null)
            notifyObservers(new FriendRequestChangeEvent(EventType.ADD, friendRequest2));
        return null;
    }

    /**
     *
     * @param id - the id of the User that responds to a friendRequest
     * @param friendId - the id of the User that sent the friendRequest
     * @param mode - accept if the User wants to accept the FriendRequest, "delete" for deleteing it
     */
    public void manageFriendRequest(Long id, Long friendId, String mode) {
        FriendRequest friendRequest = repositoryFriendRequest.getById(new Pair<>(friendId, id));
        if (friendRequest == null)
            throw new IdNotFound();
        if (!friendRequest.getId().getSecond().equals(id))
            throw new IdNotFound();
        if (mode.equals("accept")) {
            friendRequest.setStatus(Status.approved);
            repositoryFriendship.add(new Friendship(friendRequest.getId().getFirst(), friendRequest.getId().getSecond()));
        }
        else if (mode.equals("delete"))
            friendRequest.setStatus(Status.rejected);
        else
            throw new EntityNotValid("\nThe mode was not recognised (please try accept/delete)\n");
        FriendRequest friendRequest1 = repositoryFriendRequest.update(friendRequest);
        if (friendRequest1 == null)
            notifyObservers(new FriendRequestChangeEvent(EventType.UPDATE, friendRequest, friendRequest1));
    }

    /**
     *
     * @param id - the id of the User for which sent FriendRequests will be searched
     * @return a Map containing all the sent FriendRequests
     */
    public Map<User, FriendRequest> getSentFriendRequestsForUser(Long id) {
        Iterable<FriendRequest> friendRequests = repositoryFriendRequest.getAll();
        Map<User, FriendRequest> userFriendRequestMap = new HashMap<>();
        friendRequests.forEach(friendRequest -> {
            if (friendRequest.getId().getFirst().equals(id))
                userFriendRequestMap.put(repositoryUser.getById(friendRequest.getId().getSecond()), friendRequest);
        });
        return userFriendRequestMap;
    }

    public List<User> getAvailableSentFriendRequests(Long id, String username) {
        Map<Long, User> users = repositoryUser.getAllMap();
        Iterable<FriendRequest> friendRequests = repositoryFriendRequest.getAll();

        friendRequests.forEach(friendRequest -> {
            if (friendRequest.getId().getFirst().equals(id) || friendRequest.getId().getSecond().equals(id)) {
                users.remove(friendRequest.getId().getFirst());
                users.remove(friendRequest.getId().getSecond());
            }
        });
        users.remove(id);

        List<User> userList = new ArrayList<>();
        users.forEach((key, value) -> userList.add(value));
        List<User> available = new ArrayList<>();
        userList.forEach(user -> {
            if (user.getUsername().startsWith(username))
                available.add(user);
        });

        return available;
    }

    public void removeFriendRequest(Pair<Long, Long> id) {
        FriendRequest friendRequest = repositoryFriendRequest.remove(id);
        if (friendRequest == null)
            throw new ServiceException("\nThe friend request does not exist\n");
        else {
            if (friendRequest.getStatus().equals(Status.approved))
                repositoryFriendship.remove(id);
            notifyObservers(new FriendRequestChangeEvent(EventType.DELETE, friendRequest));
        }
    }

    public int getNumberReceivedFriendRequests(Long id, LocalDate startDate, LocalDate endDate) {
        Map<User, FriendRequest> friendRequests = getReceivedFriendRequestsForUser(id);
        AtomicInteger counter = new AtomicInteger();
        friendRequests.forEach((user, friendRequest) -> {
            if (friendRequest.getDate().isBefore(endDate) && friendRequest.getDate().isAfter(startDate))
                counter.getAndIncrement();
        });
        return counter.get();
    }
    public int getNumberSentFriendRequests(Long id, LocalDate startDate, LocalDate endDate) {
        Map<User, FriendRequest> friendRequests = getSentFriendRequestsForUser(id);
        AtomicInteger counter = new AtomicInteger();
        friendRequests.forEach((user, friendRequest) -> {
            if (friendRequest.getDate().isBefore(endDate) && friendRequest.getDate().isAfter(startDate))
                counter.getAndIncrement();
        });
        return counter.get();
    }

    @Override
    public void addObserver(Observer<FriendRequestChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendRequestChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendRequestChangeEvent t) {
        observers.forEach(x -> x.update(t));
    }
}
