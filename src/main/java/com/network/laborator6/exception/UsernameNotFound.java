package com.network.laborator6.exception;

public class UsernameNotFound extends RuntimeException {
    public UsernameNotFound() {
        super("\nUser with this username does not exist\n");
    }
}
