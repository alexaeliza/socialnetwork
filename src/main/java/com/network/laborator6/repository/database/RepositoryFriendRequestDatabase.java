package com.network.laborator6.repository.database;

import com.network.laborator6.domain.FriendRequest;
import com.network.laborator6.domain.Pair;
import com.network.laborator6.domain.enums.Status;
import com.network.laborator6.repository.RepositoryEntity;
import com.network.laborator6.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RepositoryFriendRequestDatabase implements RepositoryEntity<Pair<Long, Long>, FriendRequest> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryFriendRequestDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }


    /**
     *
     * @param id - the id that will be searched in the map
     * @return FriendRequest with this id if it exists, null otherwise
     */
    @Override
    public FriendRequest getById(Pair<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null\n");
        FriendRequest friendRequest = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT id1, id2, date, status FROM public.friendrequest" +
                    " WHERE id1 = ? AND id2 = ?");
            statement.setLong(1, id.getFirst());
            statement.setLong(2, id.getSecond());
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            String date = resultSet.getString("date");
            String status = resultSet.getString("status");
            friendRequest = new FriendRequest(id.getFirst(), id.getSecond(), LocalDate.parse(date, Constants.DATE_FORMATTER), Status.valueOf(status));
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendRequest;
    }

    /**
     *
     * @return an iterable object containing all FriendRequests in the database
     */
    @Override
    public Iterable<FriendRequest> getAll() {
        List<FriendRequest> friendRequests = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.friendrequest");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String date = resultSet.getString("date");
                String status = resultSet.getString("status");
                FriendRequest friendRequest = new FriendRequest(id1, id2, LocalDate.parse(date, Constants.DATE_FORMATTER), Status.valueOf(status));
                friendRequests.add(friendRequest);
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendRequests;
    }

    /**
     *
     * @param friendRequest - the FriendRequest that wll be added
     * @return null if friendRequest was successfully added, friendRequest otherwise
     */
    @Override
    public FriendRequest add(FriendRequest friendRequest) {
        if (friendRequest == null)
            throw new IllegalArgumentException("Friend Request must not be null\n");
        if (getById(friendRequest.getId()) != null)
            return friendRequest;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO public.friendrequest(id1, id2, date, status)" +
                    "VALUES (?, ?, ?, ?)");
            statement.setLong(1, friendRequest.getId().getFirst());
            statement.setLong(2, friendRequest.getId().getSecond());
            statement.setDate(3, Date.valueOf(friendRequest.getDate()));
            statement.setString(4, friendRequest.getStatus().toString());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    /**
     *
     * @param id - the id for the entity that will be deleted
     * @return the FriendRequest with this id if it was successfully deleted, null otherwise
     */
    @Override
    public FriendRequest remove(Pair<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null\n");
        FriendRequest friendRequest = getById(id);
        if (friendRequest == null)
            return null;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "DELETE FROM public.friendrequest " +
                    "WHERE id1 = ? AND id2 = ?");
            statement.setLong(1, id.getFirst());
            statement.setLong(2, id.getSecond());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendRequest;
    }

    /**
     *
     * @param friendRequest - the FrienndRequest that will be updated
     * @return null if it was successfully updated, friendRequst otherwise
     */
    @Override
    public FriendRequest update(FriendRequest friendRequest) {
        if (friendRequest == null)
            throw new IllegalArgumentException("Friend Request must not be null\n");
        if (getById(friendRequest.getId()) == null)
            return friendRequest;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE public.friendrequest SET status = ?" +
                    "WHERE id1 = ? AND id2 = ?");
            statement.setLong(2, friendRequest.getId().getFirst());
            statement.setLong(3, friendRequest.getId().getSecond());
            statement.setString(1, friendRequest.getStatus().toString());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }
}
