package com.network.laborator6.gui;

import com.network.laborator6.HelloApplication;
import com.network.laborator6.domain.User;
import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.service.ServiceChat;
import com.network.laborator6.service.ServiceFriendRequest;
import com.network.laborator6.service.ServiceFriendship;
import com.network.laborator6.service.ServiceUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    private final Long id;
    private final ServiceUser serviceUser;
    private final ServiceFriendRequest serviceFriendRequest;
    private final ServiceFriendship serviceFriendship;
    private final ServiceChat serviceChat;

    @FXML
    private TextField username;

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField city;

    @FXML
    private TextField phoneNumber;

    @FXML
    private TextField gender;

    @FXML
    private TextField birthday;

    @FXML
    private Label welcome;

    @FXML
    private Circle imageUser;

    @FXML
    private Button export;

    @FXML
    private TextField txtURLPicture;

    public HomeController(Long id, ServiceUser serviceUser, ServiceFriendRequest serviceFriendRequest, ServiceFriendship serviceFriendship, ServiceChat serviceChat) {
        this.id = id;
        this.serviceUser = serviceUser;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceFriendship = serviceFriendship;
        this.serviceChat = serviceChat;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        User user = serviceUser.searchId(this.id, (List<User>) serviceUser.getUsers());
        username.setText(user.getUsername());
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        gender.setText(user.getGender().toString());
        city.setText(user.getCity());
        phoneNumber.setText(user.getPhoneNumber());
        birthday.setText(user.getBirthday().toString());
        welcome.setText(welcome.getText() + ", " + user.getUsername());
        username.setEditable(false);
        firstName.setEditable(false);
        lastName.setEditable(false);
        birthday.setEditable(false);
        gender.setEditable(false);
        city.setEditable(false);
        phoneNumber.setEditable(false);

        UserLogin userLogin = serviceUser.findUserLoginById(this.id);
        File file = new File(userLogin.getImage());
        try {
            Image imgUser = new Image(file.toURI().toURL().toExternalForm());
            imageUser.setFill(new ImagePattern(imgUser));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void export(ActionEvent actionEvent) throws IOException {
        User user = serviceUser.searchId(this.id, (List<User>) serviceUser.getUsers());
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("statistics-view.fxml"));
        fxmlLoader.setController(new StatisticsController(serviceChat, serviceFriendship, serviceFriendRequest, user.getId(), user.getUsername()));
        Stage stage = new Stage();
        stage.setTitle("Export Statistics");
        AnchorPane anchorPane = fxmlLoader.load();
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void singleFileChooser(ActionEvent actionEvent){
        FileChooser fileChooser = new FileChooser();
        //fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPG file", "*.jpg"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG file", "*.png"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null)
            txtURLPicture.setText(file.toString());
    }
}
