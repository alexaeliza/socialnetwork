package com.network.laborator6.utils.events;

import com.network.laborator6.domain.FriendRequest;

public class FriendRequestChangeEvent implements Event {
    private final EventType eventType;
    private final FriendRequest newFriendRequest;
    private FriendRequest oldFriendRequest;

    public FriendRequestChangeEvent(EventType eventType, FriendRequest newFriendRequest) {
        this.eventType = eventType;
        this.newFriendRequest = newFriendRequest;
    }

    public FriendRequestChangeEvent(EventType eventType, FriendRequest newFriendRequest, FriendRequest oldFriendRequest) {
        this.eventType = eventType;
        this.newFriendRequest = newFriendRequest;
        this.oldFriendRequest = oldFriendRequest;
    }

    public EventType getEventType() {
        return eventType;
    }

    public FriendRequest getNewFriendRequest() {
        return newFriendRequest;
    }

    public FriendRequest getOldFriendRequest() {
        return oldFriendRequest;
    }
}
