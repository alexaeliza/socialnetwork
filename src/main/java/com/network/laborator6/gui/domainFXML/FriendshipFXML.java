package com.network.laborator6.gui.domainFXML;

import com.network.laborator6.domain.Entity;
import com.network.laborator6.domain.enums.Status;
import javafx.scene.control.Button;
import javafx.scene.shape.Circle;

import java.time.LocalDate;

public class FriendshipFXML extends Entity<Long> {
    private Circle picture;
    private String username;
    private String firstName;
    private String lastName;
    private LocalDate date;
    private Button deleteButton;

    public FriendshipFXML() {
    }

    public FriendshipFXML(Long id, String username, String firstName, String lastName, LocalDate date) {
        this.setId(id);
        this.picture = new Circle();
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        this.deleteButton = new Button("Delete");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public Circle getPicture() {
        return picture;
    }

    public void setPicture(Circle picture) {
        this.picture = picture;
    }
}
