package com.network.laborator6.repository;

import com.network.laborator6.domain.Entity;
import com.network.laborator6.exception.EntityNotValid;

public interface RepositoryEntity<ID, E extends Entity<ID>> {
    /**
     *
     * @param id - the id that will be searched in the map
     * @return the entity that has the id id
     * @throws IllegalArgumentException if the id is null
     */
    E getById(ID id);

    /**
     *
     * @return a list with all the entities
     */
    Iterable<E> getAll();

    /**
     *
     * @param entity - the entity that will be added to the map
     * @throws EntityNotValid if the entity is not valid
     */
    E add(E entity);

    /**
     *
     * @param id - the id for the entity that will be deleted
     * @throws EntityNotValid if the id is null
     */
    E remove(ID id);

    /**
     *
     * @param entity the entity that will be updated
     * @throws IllegalArgumentException if the entity is null
     */
    E update(E entity);
}
