package com.network.laborator6.domain;

import java.io.File;

public class UserLogin extends Entity<Long>{
    private String username;
    private String password;
    private String image;

    public UserLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserLogin(Long id, String username, String password) {
        this.username = username;
        this.password = password;
        this.setId(id);
    }

    public UserLogin(Long id, String username, String password, String image) {
        this.username = username;
        this.password = password;
        this.image = image;
        this.setId(id);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
