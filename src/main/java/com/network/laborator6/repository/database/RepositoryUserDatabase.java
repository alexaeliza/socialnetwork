package com.network.laborator6.repository.database;

import com.network.laborator6.domain.UserLogin;
import com.network.laborator6.domain.enums.Gender;
import com.network.laborator6.domain.User;
import com.network.laborator6.repository.RepositoryEntity;
import com.network.laborator6.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryUserDatabase implements RepositoryEntity<Long, User> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryUserDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     *
     * @return the first unused id in the database
     */
    protected Long getFirstUnusedId() {
        Map<Long, User> users = getAllMap();
        Long firstUnusedId = 1L;

        while (users.containsKey(firstUnusedId))
            ++firstUnusedId;
        return firstUnusedId;
    }

    /**
     *
     * @param id - the id that will be searched in the map
     * @return the user with this id if it exists, null otherwise
     */
    @Override
    public User getById(Long id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null");
        User user = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM public.user WHERE id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            user = extractUserFromDatabase(resultSet);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    public User getByUsername(String username){
        if (username == null)
            throw new IllegalArgumentException("Username must not be null");
        User user = null;
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM \"user\" WHERE username = ?");
            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next())
                return null;
            user = extractUserFromDatabase(resultSet);

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    /**
     *
     * @return an iterable object with all Users from the database
     */
    @Override
    public Iterable<User> getAll() {
        List<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.user");
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
                users.add(extractUserFromDatabase(resultSet));

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return users;
    }

    /**
     *
     * @param resultSet - the ResultSet containing data for a user
     * @return a User with this data
     * @throws SQLException
     */
    private User extractUserFromDatabase(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String firstName = resultSet.getString("firstname");
        String lastName = resultSet.getString("lastname");
        String username = resultSet.getString("username");
        String birthday = resultSet.getString("birthday");
        String gender = resultSet.getString("gender");
        String phoneNumber = resultSet.getString("phoneNumber");
        String city = resultSet.getString("city");
        return new User(id, firstName, lastName, username, LocalDate.parse(birthday, Constants.DATE_FORMATTER), Gender.valueOf(gender), phoneNumber, city);
    }

    /**
     *
     * @return a map containing all Users in the database
     */
    public Map<Long, User> getAllMap() {
        Map<Long, User> usersMap = new HashMap<>();
        Iterable<User> users = getAll();
        users.forEach(user -> usersMap.put(user.getId(), user));
        return usersMap;
    }

    /**
     *
     * @param user - the user that will be added
     * @return null if it was successfully added, user otherwise
     */
    @Override
    public User add(User user) {
        if (user == null)
            throw new IllegalArgumentException("User must not be null");
        if (user.getId() == null)
            user.setId(getFirstUnusedId());
        if (getById(user.getId()) != null)
            return user;
        if (getByUsername(user.getUsername()) != null)
            return user;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO public.user(id, firstname, lastname," +
                    " username, birthday, gender, phonenumber, city)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            statement.setLong(1, user.getId());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getUsername());
            statement.setDate(5, Date.valueOf(user.getBirthday()));
            statement.setString(6, user.getGender().toString());
            statement.setString(7, user.getPhoneNumber());
            statement.setString(8, user.getCity());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    /**
     *
     * @param id - the id for the entity that will be deleted
     * @return user with this id if it was successfully deleted, null otherwise
     */
    @Override
    public User remove(Long id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null");
        User user = getById(id);
        if (user == null)
            return null;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "DELETE FROM public.user WHERE id = ?");
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return user;
    }

    /**
     *
     * @param user - the User that will be updated
     * @return null if it was successfully updated, user otherwise
     */
    @Override
    public User update(User user) {
        if (user == null)
            throw new IllegalArgumentException("User must not be null");
        if (getById(user.getId()) == null)
            return user;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE public.user SET firstname = ?, " +
                    "lastname = ?, username = ?, birthday = ?, gender = ?," +
                    " phonenumber = ?, city = ? WHERE id = ?");
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getUsername());
            statement.setDate(4, Date.valueOf(user.getBirthday()));
            statement.setString(5, user.getGender().toString());
            statement.setString(6, user.getPhoneNumber());
            statement.setString(7, user.getCity());
            statement.setLong(8, user.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }
}
