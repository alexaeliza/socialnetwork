package com.network.laborator6.repository.database;

import com.network.laborator6.domain.Friendship;
import com.network.laborator6.domain.Pair;
import com.network.laborator6.repository.RepositoryEntity;
import com.network.laborator6.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RepositoryFriendshipDatabase implements RepositoryEntity<Pair<Long, Long>, Friendship> {
    private final String url;
    private final String username;
    private final String password;

    public RepositoryFriendshipDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     *
     * @param id - the id that will be searched in the map
     * @return Frienship with this id if it exists, null otherwise
     */
    @Override
    public Friendship getById(Pair<Long, Long> id) {
        Friendship friendship = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT id1, id2, date FROM public.friendship" +
                    " WHERE (id1 = ? AND id2 = ?) OR (id1 = ? AND id2 = ?)");
            statement.setLong(1, id.getFirst());
            statement.setLong(2, id.getSecond());
            statement.setLong(3, id.getSecond());
            statement.setLong(4, id.getFirst());
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            String date = resultSet.getString("date");
            friendship = new Friendship(id.getFirst(), id.getSecond(), LocalDate.parse(date, Constants.DATE_FORMATTER));
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendship;
    }

    /**
     *
     * @return a iterable object containing all Friendships from the database
     */
    @Override
    public Iterable<Friendship> getAll() {
        List<Friendship> friendships = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM public.friendship");
            while(resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                Long id2 = resultSet.getLong("id2");
                String date = resultSet.getString("date");
                Friendship friendship = new Friendship(id1, id2, LocalDate.parse(date, Constants.DATE_FORMATTER));
                friendships.add(friendship);
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendships;
    }

    /**
     *
     * @param friendship - the friendship that wll be added
     * @return null if it was successfully added, friendship otherwise
     */
    @Override
    public Friendship add(Friendship friendship) {
        if (friendship == null)
            throw new IllegalArgumentException("Friendship must not be null");
        if (getById(friendship.getId()) != null)
            return friendship;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO public.friendship(id1, id2, date)" +
                    "VALUES (?, ?, ?)");
            statement.setLong(1, friendship.getFirst());
            statement.setLong(2, friendship.getSecond());
            statement.setDate(3, Date.valueOf(friendship.getFriendshipDate()));
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }

    /**
     *
     * @param id - the id for the entity that will be deleted
     * @return the deleted Friendship if it was successfully deleted, null otherwise
     */
    @Override
    public Friendship remove(Pair<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("Id must not be null");
        Friendship friendship = getById(id);
        if (friendship == null)
            return null;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "DELETE FROM public.friendship " +
                    "WHERE (id1 = ? AND id2 = ?) OR (id1 = ? AND id2 = ?)");
            statement.setLong(1, id.getFirst());
            statement.setLong(2, id.getSecond());
            statement.setLong(3, id.getSecond());
            statement.setLong(4, id.getFirst());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return friendship;
    }

    /**
     *
     * @param friendship - the friendship that will be updated
     * @return null if it was successfully updated, friendship otherwise
     */
    @Override
    public Friendship update(Friendship friendship) {
        if (friendship == null)
            throw new IllegalArgumentException("Friendship must not be null");
        if (getById(friendship.getId()) == null)
            return friendship;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE public.friendship SET date = ? " +
                    "WHERE (id1 = ? AND id2 = ?) OR (id1 = ? AND id2 = ?)");
            statement.setDate(1, Date.valueOf(friendship.getFriendshipDate()));
            statement.setLong(2, friendship.getFirst());
            statement.setLong(3, friendship.getSecond());
            statement.setLong(4, friendship.getSecond());
            statement.setLong(5, friendship.getFirst());
            statement.execute();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            System.exit(0);
        }
        return null;
    }
}
